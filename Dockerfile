FROM python:3.9

RUN apt-get update -qy
RUN apt-get install -qy libopenblas-dev libscalapack-mpi-dev libxc-dev
RUN apt-get install -qy libelpa-dev
RUN apt-get install -qy nano vim
RUN apt-get install -qy libfftw3-dev
RUN apt-get install less

RUN adduser gpaw --disabled-password --gecos ""  #  gecos ==> non-interactive
RUN passwd --delete root
USER gpaw
WORKDIR /home/gpaw

ENV PATH /home/gpaw/.local/bin:$PATH
RUN pip -q install git+https://gitlab.com/ase/ase.git@master
RUN ase info
ADD gpaw-setups-0.9.20000.tar.gz setups

# Or get from https://wiki.fysik.dtu.dk/gpaw-files/gpaw-setups-0.9.20000.tar.gz
ENV GPAW_SETUP_PATH /home/gpaw/setups/gpaw-setups-0.9.20000

RUN pip install -U pip
RUN pip install flake8 mypy pytest pytest-xdist interrogate graphviz pyyaml types-PyYAML
RUN pip -q install git+https://gitlab.com/myqueue/myqueue.git@master

RUN git clone https://gitlab.com/gpaw/gpaw.git
ADD siteconfig.py gpaw/siteconfig.py
RUN pip install --editable gpaw
