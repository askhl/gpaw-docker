name = gpaw-test
registry = registry.gitlab.com/ase/ase
fullname = $(registry):$(name)

build:
	docker build  -t $(fullname)  .

build-no-cache:
	docker build  --no-cache -t $(fullname)  .

push:
	docker push $(fullname)

shell:
	docker run --name container-$(name) --rm -i -t $(fullname) bash

shell-no-rm:
	docker run --name container-$(name) -i -t $(fullname) bash
