fftw = True
libraries += ['fftw3']
libraries += ['openblas']

scalapack = True
libraries += ['scalapack-openmpi']
